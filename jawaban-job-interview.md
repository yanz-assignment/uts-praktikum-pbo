# 1 Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
![ppbo](./ppbo1.png)

# 2 Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
![a](./algoritma1.png)

Penjelasan :
- Pada bagian ini, program mengharapkan pengguna untuk memasukkan nomor ponsel mereka. Nomor ponsel ini kemungkinan digunakan dalam proses registrasi pengguna.

![a](./algoritma2.png)

Penjelasan : 
- String insertQuery = "INSERT INTO user (username, password, nik, nohp) VALUES (?, ?, ?, ?)"; - Mendefinisikan pernyataan SQL untuk menyisipkan (insert) data pengguna ke dalam tabel user. Parameter digunakan sebagai tanda tanya (?) yang akan diisi dengan nilai sebenarnya.
- try (PreparedStatement pst = connection.prepareStatement(insertQuery)) { - Membuat objek PreparedStatement menggunakan koneksi connection untuk mempersiapkan pernyataan SQL yang akan dieksekusi. Objek PreparedStatement memungkinkan penggunaan parameter dalam pernyataan SQL dan melindungi dari serangan injeksi SQL.
- pst.setString(1, username); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai username.

![a](./algoritma3.png)

Penjelasan :
- Pada bagian ini, program mengharapkan pengguna untuk memasukkan username dan password yang telah dibuat. Nomor ponsel ini digunakan dalam proses login pengguna.
- String loginQuery = "SELECT * FROM user WHERE username = ? AND password = ?"; - Mendefinisikan pernyataan SQL untuk melakukan pencarian data pengguna berdasarkan username dan password yang diberikan.
- pst.setString(1, loginUsername); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai loginUsername (username yang digunakan untuk login).
- ResultSet result = pst.executeQuery(); - Mengeksekusi perintah SQL untuk melakukan pencarian data pengguna berdasarkan username dan password. Perintah ini akan mengembalikan objek ResultSet yang berisi hasil pencarian.
- if (result.next()) { - Memeriksa apakah objek ResultSet memiliki baris data. Jika result.next() mengembalikan true, artinya ada baris data yang cocok dengan username dan password yang diberikan.

![a](./algoritma4.png)

Penjelasan :
- Pada bagian ini, program memberikan opsi kepada pengguna untuk memilih jenis transportasi, dengan pilihan "1. Kereta Api". Setelah pengguna memilih pilihan transportasi tersebut, program akan melanjutkan dengan meminta beberapa informasi tambahan untuk memesan tiket kereta api.
- Program akan menampilkan pertanyaan "Dari : " di mana pengguna diharapkan memasukkan lokasi keberangkatan. Kemudian, program akan menampilkan pertanyaan "Ke : " di mana pengguna diminta untuk memasukkan lokasi tujuan perjalanan.
- Selanjutnya, program akan menampilkan "Mode perjalanan" dengan opsi "1. Sekali jalan" dan "2. Pulang pergi". Pengguna diminta untuk memilih salah satu opsi tersebut dengan memasukkan angka yang sesuai.
- Setelah itu, program akan meminta pengguna untuk memasukkan jumlah penumpang dengan menampilkan pertanyaan "Jumlah Penumpang : ".

![a](./algoritma5.png)

Penjelasan :
- Pengguna diminta memasukkan tanggal pergi, nama kereta dan nomor kursi.
- String selectQuery = "SELECT username, nik, nohp FROM user WHERE username = ?"; - Mendefinisikan pernyataan SQL untuk mengambil data pengguna dari tabel user berdasarkan username yang diberikan. Pernyataan ini akan mengambil kolom username, nik, dan nohp dari baris pengguna yang sesuai.
- get.setString(1, loginUsername); - Mengatur nilai parameter pertama (?) dalam pernyataan SQL dengan nilai loginUsername (username yang digunakan untuk mencari data pengguna).
- String usernameDB = resultset.getString("username"); - Mengambil nilai kolom "username" dari baris hasil pencarian dan menyimpannya ke dalam variabel usernameDB.

![a](./algoritma6.png)

Penjelasan :
- Pada bagian ini, program menangani sistem pembayaran untuk tiket kereta api. Pertama, program akan menampilkan total pembayaran yang harus dilakukan oleh pengguna dengan menggunakan pernyataan System.out.println("Total Pembayaran Rp." + hargaTiket);. Variabel hargaTiket mengandung nilai total pembayaran yang dihitung sebelumnya.
- Selanjutnya, program akan meminta pengguna untuk memasukkan nominal bayar dengan menampilkan pernyataan System.out.print("Masukkan nominal bayar Rp."); dan mengambil input dari pengguna menggunakan input.nextInt() untuk menyimpan nilai yang dimasukkan ke dalam variabel nominalBayar.
- Setelah itu, program melakukan pengecekan apakah nominal bayar yang dimasukkan oleh pengguna cukup untuk membayar tiket atau tidak. Jika nominalBayar kurang dari hargaTiket, maka program akan menampilkan pesan "E-Ticket tidak cukup!" menggunakan pernyataan System.out.println("E-Ticket tidak cukup!");. Ini menunjukkan bahwa nominal bayar yang dimasukkan tidak mencukupi untuk membayar tiket.

# 3 Mampu menjelaskan konsep dasar OOP
- Class, merupakan "template" untuk membuat objek yang didalamnya mendifinisikan attribute dan metode.
- Objek, merupakan instance dari suatu kelas yang memiliki akses ke attribute dan metode yand didefinisikan didalamnya.
- Inheritance, merupakan pembuatan kelas baru yang diturunkan dari kelas induk yang mewarisi semua attribute dan metode super class.
- Enkapsulasi, adalah konsep dalam oop yang melibatkan penyembunyian data dan implementasi detail suatu objek dari akses langsung oleh objek lain.
- Polymorphism, kemampuan objek yang berbededa untuk menggunakan metode yang sama dengan cara yang berbeda.
- Abstraksi, melibatkan penyembunyian rincian implementasi yang kompleks dari objek.

# 4 Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)
![Enkapsulasi](./enkapsulasi.png)

# 5 Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)
![Abstraksi](./abstraksi.png)

# 6 Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait
Inheritance

![inheritance](./pewarisan.png)

Polymorphism

![polymorphism](./polimorpisem.png)

# 7 Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

|User|Bisa|Nilai|
|----|----|-----|
|User|Pesan Tiket Kereta Api|100|
User|Pesan Tiket Pesawat|99
User|Pesan Sewa Mobil|98
User|Pesan Tiket Hotel|97
User|Memilih Fasilitas Bagasi Pesawat Internasional|80
User|Check in Pesawat Internasional|79
User|Check in Pesawat Domestik|78
User|Memilih Fasilitas Bagasi Pesawat Domestik|77
User|Mengetahui Syarat dan ketentuan Penerbangan Domestik|76
User|Memilih List Maskapai Domestik yang tersedia|75
User|Online Check in tiket.com|74
User|Mendapat Asuransi Pesawat|73
User|Klaim Asuransi Pesawat Domestik|72
User|Melihat info umum tiket hotel|71
User|Melihat informasi waktu check in dan check out hotel|70
User|Konsultasi kendala pemesanan tiket hotel|70
User|Memilih fasilitas tambahan pemesanan hotel|70
User|Bayar di hotel|70
User|Konsultasi e Tiket tidak terbit|70
User|Mendapat asuransi hotel|70
User|Menulis ulasan hotel|70
User|Chat dengan hotel|70
User|Menghubungi dan menemukan lokasi hotel|70
User|Mengetahui informasi umum ticket To Do|70
User|Pemesanan To Do|60
User|Cara mengakses tiket Live|60
User|Sewa video di tiket.com|60
User|Pesan tiket To Do di tiket.com|60
User|Konsultasi perubahan event mengalami perubahan|60
User|Mengetahui informasi tiket JR Pass|60
User|Penukaran To Do|60
User|Mengetahui info kontak dan lokasi To Do|60
User|Penukaran tiket To Do di tiket.com|60
User|Mengetahui ketentuan pemesanan tiket kereta api di tiket.com|50
User|Mendapat asuransi perjalanan kereta api di tiket.com|50
User|Membayar biaya tambahan sewa mobil|50
User|Mengetahui ketentuan umum sewa mobil|50
User|Pesan jemputan bandara|50
User|Mengetahui lokasi jemputan bandara|50
User|Mengetahui ketentuan umum pemesanan jemputan bandara|50
User|Pemesanan bus dan travel|40
User|Mengetahui informasi pembayaran produk tiket.com|40
User|Mengetahui mata uang yang tersedia untuk pembayaran tiket.com|30
User|Membayar dengan ATM|30
User|Membayar dengan kartu debit|30
User|Membayar dengan e Wallet|30
User|Membayar dengan Transfer bank|30
User|Membayar dengan Virtual Account|30
User|Membayar Instan|30
User|Membayar dengan gerai retail|30
User|Membayar dengan cicilan tanpa kartu kredit|30
User|Mengetahui informasi kartu kredit|30
User|Menyimpan dan menghapus data kartu kredit|30
User|Pengajuan pay later by indodana|30
User|Pengajuan kartu kredit BCA by tiket.com|30
User|Mengaktifkan akun Blipay di tiket.com jika sudah memiliki akun Blipay|30
User|Mengaktifkan akun Blipay di tiket.com jika belum memiliki akun Blipay|30
User|Refund dan Withdraw Blipay|30
User|Menonaktifkan Blipay di tiket.com|30
User|Membatalkan Tiket Pesawat Internasional|20
User|Cek status proses refund Pesawat Internasional|20
User|Mengubah jadwal / reschedule tiket pesawat internasional yang sudah dipesan|20
User|Mendapatkan informasi perubahan jadwal dari maskapai internasional|20
User|Pembatalan tiket pesawat domestik|20
User|Cek status proses refund pesawat domestik|20
User|Mengubah jadwal / reschedule tiket pesawat domestik yang sudah dipesan|20
User|Mendapatkan informasi perubahan jadwal dari maskapai domestik|20
User|Pembatalan pemesanan tiket hotel non - refundable|20
User|Mengajukan reschedule tiket hotel|20
User|Pembatalan tiket hotel|20
User|Perubahan jadwal pemesanan tiket hotel non - refundable|20
User|Cek status refund tiket hotel|20
User|Perubahan dan pengembalian dana atau refund tiket To Do|20
User|Ubah pemesanan tiket kereta|20
User|Pembatalan / refund pemesanan tiket kereta api|20
User|Perubahan jadwal sewa mobil|20
User|Pembatalan sewa mobil|20
User|Refund pemesanan jemputan bandara|20
User|Mengubah pemesanan jemputan bandara|20
User|Refund dan schedule bus dan travel|20
User|Pendaftaran akun tiket.com|10
User|Perubahan informasi tiket.com|10
User|Penghapusan akun|10
User|Menjaga keamanan akun|10
User|Menggunakan promo dan gift voucher|10
User|Menggunakan tiket.com referral|10
User|Mendapatkan tiket CLEAN|10
User|Mengajukan klaim Jaminan Harga Termurah|10
User|Mendapatkan layanan tiket Plus|10
User|Mengajukan kerja sama dengan tiket.com|10
User|Mendapatkan Blibli tiket rewards|10
User|Mendapatkan Blibli tiket points|10
User|Koreksi data penumpang pesawat internasional|10
User|Koreksi data penumpang pesawat domestik|10
User|Ubah nama pemesan hotel|10
User|Ganti permintaan khusus pemesanan hotel|10
User|Perubahan data pemesanan sewa mobil|10
Manajemen|Memeriksa pembayaran yang sudah masuk|10
Manajemen|Mengumpulkan data pelanggan|10
Manajemen|Meningkatkan efisiensi operasional|10
Manajemen|Meningkatkan pengalaman pelanggan|10
Manajemen|Meningkatkan keuangan yang lebih efektif|10
Manajemen|Mempromosikan dan memasarkan|10
Direksi|Menyediakan kanal pembayaran tambahan|10
Direksi|Meningkatkan efisiensi dan pengendalian|10
Diresi|Mengambil keputusan berdasarkan data|10
Direksi|Meningkatkan keamanan|10
Direksi|Mengembangkan inovasi|10

# 8 Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)
![classD](./classDiagram.png)

Penjelasan : 
- 'KeretaApiBuilder' hanya digunakan untuk membangun objek 'KeretaApi', tetapi tidak memilikinya sebagai atribut atau bagian dari komposisi, maka hubungan tersebut dapat direpresentasikan dengan hubungan agregasi.
- Dalam diagram diatas, menunjukkan bahwa 'RegulerUser' memiliki ketergantungan pada objek 'KeretaApi'. Hubungan kelas tersebut menujukkan hubungan asosiasi.
- Dalam class diagram, hubungan antara User sebagai kelas abstrak dengan RegularUser, GuestUser, dan KeretaApi sebagai kelas turunan dapat direpresentasikan dengan hubungan pewarisan atau inheritance. Hubungan ini menunjukkan bahwa RegularUser, GuestUser, dan KeretaApi adalah kelas yang mewarisi sifat atau perilaku dari kelas abstrak User.
- Dalam class diagram, hubungan antara TiketWallet dan 'kereta api' setelah pembayaran berhasil dapat direpresentasikan dengan sebuah hubungan ketergantungan (dependency). Hubungan ini menunjukkan bahwa TiketWallet bergantung pada fungsi pencetakan tiket kereta api setelah pembayaran berhasil dilakukan.

# 9 Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)
[Klik disini](https://www.youtube.com/watch?v=p58-L6-gCzE)

# 10 Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
![ux](./mainmenu.png)
